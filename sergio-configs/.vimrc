set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required

Plugin 'VundleVim/Vundle.vim'
"Plugin 'christoomey/vim-tmux-navigator'
"Plugin 'jamestomasino/vim-writeroom'

Plugin 'jceb/vim-orgmode'

"setlocal spell spelllang=en_us
setlocal tw=165
setlocal nu
setlocal spellfile=$HOME/.vim.spell.add
syntax on

setlocal expandtab
setlocal tabstop=4

" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin indent on

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" this is mostly a matter of taste. but LaTeX looks good with just a bit
" of indentation.
set sw=2

" TIP: if you write your \label's as \label{fig:something}, then if you
" type in \ref{fig: and press <C-n> you will automatically cycle through
" all the figure labels. Very useful!
set iskeyword+=:


let c='a'
while c <= 'z'
   exec "set <A-".c.">=\e".c
   exec "imap \e".c." <A-".c.">"
   let c = nr2char(1+char2nr(c))
endw

set timeoutlen=0 ttimeoutlen=0
