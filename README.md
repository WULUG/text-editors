# Wulug Text Editor Repository
Text Configurations for Vim and Emacs are here
Feel free to contribute for other Editors as well

## Current Contributors
Adam Sweeney  
Amey Shukla  
Dennis Pham  
Sergio Salinas  

## Getting the submodule(s)
After cloning this repository, `cd` into it and run the following command: `git submodule init && git submodule update`.

### Contact wulug@wichita.edu to contribute
