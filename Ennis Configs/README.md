# Emacs Help

Keep in mind that the config files are supposed to be hidden and out of your way.
However by default emacs looks in your home directory for these files.

## Copying the emacs config files to your home directory

To copy these files over you'll have to run a couple of copy commands.
typically you'll type something like this `cp -r .emacs* $HOME` or `cp -r .emacs* ~`

### Optional: Disecting the cp command
#### -r

-r is a recursive flag. Because there is an .emacs.d/ folder, the copy must be done
recursively. If you want to see what it does in "_real_" time you can use -rv instead.

#### the asterisk

the asterisk is a handy thing to use when running commands. Notice that the .emacs file
itself and the .emacs.d/ folder have similar names up to a point. By using the * at the
end of .emacs\* we're telling the system to run this command that matches anything with
the word .emacs in front of it.

#### $HOME and ~(tilda)

Later on, when we get around to it, we'll cover scripting. But in short $HOME is one of
many specific types of variables used in bash scripting. It just represents a path
to your home directory. The tilda accomplishes a similar thing but it's more of a macro
than a variable.

## Using the emacs config

Once you have the files copied to your home directory the emacs text editor will use it.

## Some things to be aware of

This emacs config was made by me for me. So if you're planning on using it be ready to
deal with my style of programming. You're still free to change it and the comments are there
to help you do so.

### Syntax guide
`C-` means Control<br />
`M-` means Alt<br />

---

If there is a space between two bindings like `C-x s` it means they're two different actions
in the `C-x s` example you would first press Control x and then press s.

## Common Defaults

If you are new to emacs here are some common bindings that you might want to use.
_get ready for a swole pinky_

### Open and Save files
`C-x C-f` find a file, it opens files and directories based on a path given to it.<br />
![show me](/markdown_imgs/show_me_a_file.png) <br />
`C-x s` Save all _you should have a prompt at the bottom of your emacs screen to really do it or not_<br />
`C-x C-s` Save current file/buffer<br />
`C-x k` kill buffer _this closes your file in emacs_<br />

### Switching Buffers
`C-x left` switch buffer to whatever is queued to the left of your current buffer<br />
`C-x right` switch buffer to whatever is queued to the right<br />

### Manipulating Buffers
`C-x 3` create a vertical mini buffer<br />
`C-x 2` create a horizontal mini buffer<br />
`C-x 0` close mini buffer<br />
`C-x o` switch mini buffer<br />

### Search (and replace)
`C-s` search for a word or something. press `C-s` again to find the next occurrence<br />
`C-M-%` find and replace, give it a word or phrase to find and give it another one to replace it with.
it will ask you if you want to replace said word and highlight it every time it finds something. you
can either press `n` for no or `y` for yes.

### Copy and Paste
`C-w` cut<br />
`M-w` copy<br />
`C-y` paste<br />

## Undoing my chicanery and command mode

You'll probably notice that my menu and tool bar are missing.<br />
To bring this back you'll have to enter a command in command mode. To enter command mode you'll press
`M-x` then emacs will start accepting commands.<br />
![command mode](/markdown_imgs/command_mode.png)<br />
To bail out of this you can mash the `esc` key. Normally
you'd have to press `C-g` which will still work, but I found it unintuitive and mapped it to `esc` for 
your convenience.<br />
Anyways back to bringing the tool and menu bars back<br />
The command you're looking for is `menu-bar-mode` and `tool-bar-mode`<br />
you can also remove the code I have in the .emacs file that removes them as well.<br />
![delet this](/markdown_imgs/delet_this.png)

## Bonus bindings and features

`M-up` Moves the current line you are on up one line<br />
`M-down` Moves the current line you are on down one line<br />
`Tab` Will either auto complete or cycle through words that start with the start of the word you are on<br />

#### Slight changes
`.h` files are now configured to use the C++ style syntax. I did this because a lot of engines and source code
likes to keep using `.h` as their header files.<br />

Indentation is set to 4 spaces. 

`.m` files are set as MatLab files and not Objective-C files

Haskell support added<br />
Python support added<br />
C# support added<br />

#### What `C-x 3` looks like
_before_
![vsp before](/markdown_imgs/vsp_before.png)
_after_
![vsp after](/markdown_imgs/vsp_after.png)

#### What `C-x 2` looks like
_before_
![vsp before](/markdown_imgs/vsp_before.png)
_after_
![hz after](/markdown_imgs/hz_after.png)

#### Using them together
_oof_
![before horror](/markdown_imgs/before_horror.png)
_owie_
![oh no](/markdown_imgs/oh_no.png)
_my bones_
![stahp](/markdown_imgs/stahp.png)


