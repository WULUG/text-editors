(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar EnnisPack
  '(evil
    evil-mc
    evil-nerd-commenter
    go-mode
    rust-mode
    python-mode
    py-autopep8
    haskell-mode
    dart-mode
    focus
    color-theme-sanityinc-tomorrow
    dimmer
    ranger
    )) 
    
(mapc #'(lambda (package)
          (unless (package-installed-p package)
            (package-install package)))
          EnnisPack)

(load-theme 'sanityinc-tomorrow-eighties t)
(add-to-list 'default-frame-alist
	     '(font . "Droid Sans Mono-11"))

;; Evil Configs
(require 'evil)
(evil-mode 1)

(require 'evil-mc)
(global-evil-mc-mode 1)

(global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)

(load "~/.emacs.d/src/multicur.el")
(load "~/.emacs.d/src/ux.el")

;; disable autosaves and backups
(setq make-backup-files nil)
(setq auto-save-default nil)

(define-key key-translation-map (kbd "ESC") (kbd "C-g"))

;; Line movement
(load "~/.emacs.d/src/moveline.el")
(global-set-key [(M-up)] 'move-line-up)
(global-set-key [(M-down)] 'move-line-down)

;; Vanilla rebinds
(load "~/.emacs.d/src/vanilla_keybinds.el")

;; Ennis' c++ stuff
(load "~/.emacs.d/src/clong.el")
(add-to-list 'magic-fallback-mode-alist
	     '(buffer-standard-include-p . c++-mode))
(add-hook 'c++-mode-hook 'ennis-c++-hook)

;; C-tags!
(load "~/.emacs.d/src/ctogs.el")

;; smart tabs
(load "~/.emacs.d/src/autocompl.el")

;; remove default buffers
(load "~/.emacs.d/src/no_defaults.el")

;; some bullshit done by the load-theme command
