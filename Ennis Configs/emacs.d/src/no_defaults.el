(setq-default message-log-max nil)

(if (get-buffer "*Messages*")
    (kill-buffer "*Messages*"))

(add-hook 'minibuffer-exit-hook
	  '(lambda ()
	     (let ((buffer "*Completions*"))
	       (and (get-buffer buffer)
		    (kill-buffer buffer)))))

(setq inhibit-startup-buffer-menu t)
