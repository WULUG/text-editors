;; buffer tabbing
(global-set-key [M-tab] 'next-buffer)
(global-set-key [M-iso-lefttab] 'previous-buffer)

;; window tabbing
(global-set-key [C-tab] 'other-window)

;; jump to line
(global-set-key (kbd "C-c l") 'goto-line)
