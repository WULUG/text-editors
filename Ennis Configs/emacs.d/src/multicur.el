(global-set-key (kbd "C-x C-n") 'evil-mc-make-and-goto-next-match)
(global-set-key (kbd "C-x C-s") 'evil-mc-skip-and-goto-next-match)
(global-set-key (kbd "C-x C-q") 'evil-mc-undo-all-cursors)
(global-set-key (kbd "C-x l") 'evil-mc-undo-last-added-cursor)
