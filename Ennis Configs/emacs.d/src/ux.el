(setq inhibit-startup-message t)
(global-linum-mode t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

;; turn off that fucking bell
(setq ring-bell-function 'ignore)

;; truncate long lines
(set-default 'truncate-lines t) 

(require 'dimmer)
(dimmer-mode)

(global-set-key (kbd "C-x f") 'focus-mode)
