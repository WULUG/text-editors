(setq c-default-style "c++")

(add-to-list
 'auto-mode-alist
 '("\\.h\\'" . c++-mode))

(add-to-list
 'auto-mode-alist
 '("\\.c\\'" . c++-mode))

(add-to-list
 'auto-mode-alist
 '("\\.icc\\'" . c++-mode))


(add-to-list
 'auto-mode-alist
 '("\\.tcc\\'" . c++-mode))

(c-add-style "c++"
	     '("stroustrup"
	       (indent-tabs-mode . nil) ; no automatic indentation
	       (c-basic-offset . 4) ; indentation set to 4 spaces
	       (c-offsets-alist . ((inline-open . 0)
				   (statement-case-open . +))))) ; automatic brace, paren, and brackets disabled.
(defun ennis-c++-hook ()
  (c-set-style "c++")
  (auto-fill-mode)
  (c-toggle-auto-hungry-state 0))
