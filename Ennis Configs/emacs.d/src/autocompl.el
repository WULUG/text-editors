;;;;;;;;;;;;;;;
;; smart-tab ;;  Provided by Dustinos
;;;;;;;;;;;;;;;

;; What the fuck did you just bring to this cursed land
 
(require 'easy-mmode)
 
(eval-when-compile
  ;; Forward declaration, does not define variable
  (defvar auto-complete-mode))
 
(defgroup smart-tab nil
  "Options for `smart-tab-mode'."
  :group 'tools)
 
(defvar smart-tab-debug nil
  "Turn on for logging about which `smart-tab' function ends up being called.")
 
(defcustom smart-tab-using-hippie-expand nil
  "Use `hippie-expand' to expand text.
Use either `hippie-expand' or `dabbrev-expand' for expanding text
when we don't have to indent."
  :type '(choice
          (const :tag "hippie-expand" t)
          (const :tag "dabbrev-expand" nil))
  :group 'smart-tab)
 
(defcustom smart-tab-completion-functions-alist
  '((emacs-lisp-mode . lisp-complete-symbol)
    (text-mode       . dabbrev-completion))
  "A-list of major modes in which to use a mode specific completion function.
If current major mode is not found in this alist, fall back to
`hippie-expand' or `dabbrev-expand', depending on the value of
`smart-tab-using-hippie-expand'"
  :type '(alist :key-type (symbol :tag "Major mode")
                :value-type (function :tag "Completion function to use in this mode"))
  :group 'smart-tab)
 
(defcustom smart-tab-disabled-major-modes '(org-mode term-mode eshell-mode w3m-mode magit-mode)
  "List of major modes that should not use `smart-tab'."
  :type 'sexp
  :group 'smart-tab)
 
(put 'smart-tab-funcall 'lisp-indent-function 0)
(put 'smart-tab-funcall 'edebug-form-spec '(body))
(defmacro smart-tab-funcall (function &rest args)
  "If FUNCTION is `fboundp' call it with ARGS."
  `(let ((function ,function))
     (if (fboundp function)
         (apply function ,@args nil))))
 
(defun smart-tab-call-completion-function ()
  "Get a completion function according to current major mode."
  (if smart-tab-debug
      (message "complete"))
  (let ((completion-function
         (cdr (assq major-mode smart-tab-completion-functions-alist))))
    (if (null completion-function)
        (if (and (not (minibufferp))
                 (memq 'auto-complete-mode minor-mode-list)
                 (boundp' auto-complete-mode)
                 auto-complete-mode)
            (smart-tab-funcall 'ac-start :force-init t)
          (if smart-tab-using-hippie-expand
              (hippie-expand nil)
            (dabbrev-expand nil)))
      (funcall completion-function))))
 
(defun smart-tab-must-expand (&optional prefix)
  "If PREFIX is \\[universal-argument] or the mark is active, do not expand.
Otherwise, uses the user's preferred expansion function to expand
the text at point."
  (unless (or (consp prefix)
              (use-region-p))
    (looking-at "\\_>")))
 
(defun smart-tab-default ()
  "Indent region if mark is active, or current line otherwise."
  (interactive)
  (if smart-tab-debug
      (message "default"))
  (let* ((smart-tab-mode nil)
         (global-smart-tab-mode nil)
         (ev last-command-event)
         (triggering-key (cl-case (type-of ev)
                           (integer (char-to-string ev))
                           (symbol (vector ev))))
         (original-func (or (key-binding triggering-key)
                            (key-binding (lookup-key local-function-key-map
                                                     triggering-key))
                            'indent-for-tab-command)))
    (call-interactively original-func)))
 
;;;###autoload
(defun smart-tab (&optional prefix)
  "Try to 'do the smart thing' when tab is pressed.
`smart-tab' attempts to expand the text before the point or
indent the current line or selection.
In a regular buffer, `smart-tab' will attempt to expand with
either `hippie-expand' or `dabbrev-expand', depending on the
value of `smart-tab-using-hippie-expand'.  Alternatively, if
`auto-complete-mode' is enabled in the current buffer,
`auto-complete' will be used to attempt expansion.  If the mark
is active, or PREFIX is \\[universal-argument], then `smart-tab'
will indent the region or the current line (if the mark is not
active)."
  (interactive "P")
  (cond
   (buffer-read-only
    (smart-tab-default))
   ((use-region-p)
    (indent-region (region-beginning)
                   (region-end)))
   ((smart-tab-must-expand prefix)
    (smart-tab-call-completion-function))
   (t
    (smart-tab-default))))
 
;;;###autoload
(defun smart-tab-mode-on ()
  "Turn on `smart-tab-mode'."
    (smart-tab-mode 1))
 
(defun smart-tab-mode-off ()
  "Turn off `smart-tab-mode'."
  (smart-tab-mode -1))
 
;;;###autoload
(define-minor-mode smart-tab-mode
  "Enable `smart-tab' to be used in place of tab.
With no argument, this command toggles the mode.
Non-null prefix argument turns on the mode.
Null prefix argument turns off the mode."
  :lighter " Smrt"
  :group 'smart-tab
  :require 'smart-tab
  :keymap '(("\t" . smart-tab)
            ([(tab)] . smart-tab))
  (if smart-tab-mode
      (progn
        ;; Don't start `smart-tab-mode' when in the minibuffer or a read-only
        ;; buffer.
        (when (or (minibufferp)
                  buffer-read-only
                  (member major-mode smart-tab-disabled-major-modes))
          (smart-tab-mode-off)))))
 
;;;###autoload
(define-globalized-minor-mode global-smart-tab-mode
  smart-tab-mode
  smart-tab-mode-on
  :group 'smart-tab)
 
(provide 'smart-tab)
 
(require 'smart-tab)
(global-smart-tab-mode 1)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
