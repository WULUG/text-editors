;; init.el --- Emacs configuration

;; INSTALL PACKAGES
;; --------------------------------------

(require 'package)

(add-to-list 'package-archives
       '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(better-defaults
    elpy
    flycheck
    material-theme
    py-autopep8
    clang-format
    modern-cpp-font-lock
    irony
    company-irony
    flycheck-irony
    exwm
    fsharp-mode))


(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

;; BASIC CUSTOMIZATION
;; --------------------------------------

(setq inhibit-startup-message t) ;; hide the startup message
(load-theme 'material t) ;; load material theme
(global-linum-mode t) ;; enable line numbers globally
(menu-bar-mode -1) ;; disable menu bar
(tool-bar-mode -1) ;;Disable tool bar mode
(toggle-scroll-bar -1) ;;Disable scroll bar mode
(setq make-backup-files nil) ;;Disable backup name files
;; (require 'exwm)  ;;Disabled by default. Remove comment to enable
;; (require 'exwm-config) ;: 
;; (exwm-config-default)


;;Python PACKAGES
;;----------------------------------------

(elpy-enable)
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;;C++ PACKAGES
;;-----------------------------------------

(global-set-key (kbd "C-c C-f") 'clang-format-region)
(add-hook 'c++-mode-hook 'flycheck-mode) ;; flycheck hook
(modern-c++-font-lock-global-mode t)
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;;F# PACKAGES
;;---------------------------------------------(require 'fsharp-mode)


;;-------------------------

;; init.el ends here


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (exwm ## flycheck elpy material-theme haskell-mode better-defaults))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
